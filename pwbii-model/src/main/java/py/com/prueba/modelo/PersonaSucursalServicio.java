/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guille
 */
@Entity
@Table(name = "persona_sucursal_servicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonaSucursalServicio.findAll", query = "SELECT p FROM PersonaSucursalServicio p")
    ,
    @NamedQuery(name = "PersonaSucursalServicio.findByIdPersonaSucursalServicio", query = "SELECT p FROM PersonaSucursalServicio p WHERE p.idPersonaSucursalServicio = :idPersonaSucursalServicio")
    ,
    @NamedQuery(name = "PersonaSucursalServicio.findByPrecio", query = "SELECT p FROM PersonaSucursalServicio p WHERE p.precio = :precio")})
public class PersonaSucursalServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_persona_sucursal_servicio")
    private Integer idPersonaSucursalServicio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private BigDecimal precio;
    @JsonIgnore
    @JoinColumn(name = "id_sucursal_servicio", referencedColumnName = "id_sucursal_servicio")
    @ManyToOne(optional = false)
    private SucursalServicio idSucursalServicio;
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_persona")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Persona idEmpleado;

    public PersonaSucursalServicio() {
    }

    public PersonaSucursalServicio(Integer idPersonaSucursalServicio) {
        this.idPersonaSucursalServicio = idPersonaSucursalServicio;
    }

    public Integer getIdPersonaSucursalServicio() {
        return idPersonaSucursalServicio;
    }

    public void setIdPersonaSucursalServicio(Integer idPersonaSucursalServicio) {
        this.idPersonaSucursalServicio = idPersonaSucursalServicio;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public SucursalServicio getIdSucursalServicio() {
        return idSucursalServicio;
    }

    public void setIdSucursalServicio(SucursalServicio idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public Persona getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Persona idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersonaSucursalServicio != null ? idPersonaSucursalServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaSucursalServicio)) {
            return false;
        }
        PersonaSucursalServicio other = (PersonaSucursalServicio) object;
        if ((this.idPersonaSucursalServicio == null && other.idPersonaSucursalServicio != null) || (this.idPersonaSucursalServicio != null && !this.idPersonaSucursalServicio.equals(other.idPersonaSucursalServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.PersonaSucursalServicio[ idPersonaSucursalServicio=" + idPersonaSucursalServicio + " ]";
    }

}
