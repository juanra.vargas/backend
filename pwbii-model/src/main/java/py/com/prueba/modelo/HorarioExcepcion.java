/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guille
 */
@Entity
@Table(name = "horario_excepcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HorarioExcepcion.findAll", query = "SELECT h FROM HorarioExcepcion h")
    ,
    @NamedQuery(name = "HorarioExcepcion.findByIdHorarioExcepcion", query = "SELECT h FROM HorarioExcepcion h WHERE h.idHorarioExcepcion = :idHorarioExcepcion")
    ,
    @NamedQuery(name = "HorarioExcepcion.findByFecha", query = "SELECT h FROM HorarioExcepcion h WHERE h.fecha = :fecha")
    ,
    @NamedQuery(name = "HorarioExcepcion.findByHoraApertura", query = "SELECT h FROM HorarioExcepcion h WHERE h.horaApertura = :horaApertura")
    ,
    @NamedQuery(name = "HorarioExcepcion.findByHoraCierre", query = "SELECT h FROM HorarioExcepcion h WHERE h.horaCierre = :horaCierre")})
public class HorarioExcepcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_horario_excepcion")
    private Integer idHorarioExcepcion;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date horaApertura;
    @Basic(optional = false)
    @Column(name = "hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date horaCierre;
    @JoinColumn(name = "id_sucursal", referencedColumnName = "id_sucursal")
    @ManyToOne(optional = false)
    private Sucursal idSucursal;

    public HorarioExcepcion() {
    }

    public HorarioExcepcion(Integer idHorarioExcepcion) {
        this.idHorarioExcepcion = idHorarioExcepcion;
    }

    public HorarioExcepcion(Integer idHorarioExcepcion, Date fecha, Date horaApertura, Date horaCierre) {
        this.idHorarioExcepcion = idHorarioExcepcion;
        this.fecha = fecha;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
    }

    public Integer getIdHorarioExcepcion() {
        return idHorarioExcepcion;
    }

    public void setIdHorarioExcepcion(Integer idHorarioExcepcion) {
        this.idHorarioExcepcion = idHorarioExcepcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(Date horaApertura) {
        this.horaApertura = horaApertura;
    }

    public Date getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(Date horaCierre) {
        this.horaCierre = horaCierre;
    }

    public Sucursal getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Sucursal idSucursal) {
        this.idSucursal = idSucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHorarioExcepcion != null ? idHorarioExcepcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioExcepcion)) {
            return false;
        }
        HorarioExcepcion other = (HorarioExcepcion) object;
        if ((this.idHorarioExcepcion == null && other.idHorarioExcepcion != null) || (this.idHorarioExcepcion != null && !this.idHorarioExcepcion.equals(other.idHorarioExcepcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.HorarioExcepcion[ idHorarioExcepcion=" + idHorarioExcepcion + " ]";
    }

}
