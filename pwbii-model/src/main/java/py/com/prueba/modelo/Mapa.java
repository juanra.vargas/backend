/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guille
 */
@Entity
@Table(name = "mapa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mapa.findAll", query = "SELECT m FROM Mapa m")
    ,
    @NamedQuery(name = "Mapa.findByIdMapa", query = "SELECT m FROM Mapa m WHERE m.idMapa = :idMapa")
    ,
    @NamedQuery(name = "Mapa.findByNombre", query = "SELECT m FROM Mapa m WHERE m.nombre = :nombre")
    ,
    @NamedQuery(name = "Mapa.findByDireccion", query = "SELECT m FROM Mapa m WHERE m.direccion = :direccion")
    ,
    @NamedQuery(name = "Mapa.findByLatitud", query = "SELECT m FROM Mapa m WHERE m.latitud = :latitud")
    ,
    @NamedQuery(name = "Mapa.findByLongitud", query = "SELECT m FROM Mapa m WHERE m.longitud = :longitud")})
public class Mapa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_mapa")
    private Integer idMapa;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitud")
    private BigDecimal latitud;
    @Column(name = "longitud")
    private BigDecimal longitud;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMapa")
    private Collection<Sucursal> sucursalCollection;

    public Mapa() {
    }

    public Mapa(Integer idMapa) {
        this.idMapa = idMapa;
    }

    public Mapa(Integer idMapa, String nombre, String direccion) {
        this.idMapa = idMapa;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Integer getIdMapa() {
        return idMapa;
    }

    public void setIdMapa(Integer idMapa) {
        this.idMapa = idMapa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public BigDecimal getLatitud() {
        return latitud;
    }

    public void setLatitud(BigDecimal latitud) {
        this.latitud = latitud;
    }

    public BigDecimal getLongitud() {
        return longitud;
    }

    public void setLongitud(BigDecimal longitud) {
        this.longitud = longitud;
    }

    @XmlTransient
    public Collection<Sucursal> getSucursalCollection() {
        return sucursalCollection;
    }

    public void setSucursalCollection(Collection<Sucursal> sucursalCollection) {
        this.sucursalCollection = sucursalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMapa != null ? idMapa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mapa)) {
            return false;
        }
        Mapa other = (Mapa) object;
        if ((this.idMapa == null && other.idMapa != null) || (this.idMapa != null && !this.idMapa.equals(other.idMapa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.Mapa[ idMapa=" + idMapa + " ]";
    }

}
