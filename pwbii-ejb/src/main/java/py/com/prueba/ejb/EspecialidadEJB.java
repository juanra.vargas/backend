/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.Servicio;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;

/**
 *
 * @author juanvargas2096
 */
@Stateless
public class EspecialidadEJB {

    @PersistenceContext(unitName = "pwbiiPU")
    private EntityManager em;
    @EJB
    private SucursalEJB sucursalEJB;

    protected EntityManager getEm() {
        return em;
    }

    public EspecialidadEJB() {
    }

    public Especialidad get(Integer id) {
        return (Especialidad) getEm().find(Especialidad.class, id);
    }

    public TreeMap listar(Integer idSucursal) {

        TreeMap<String, Object> h = new TreeMap<>();
        Sucursal sucursal = sucursalEJB.get(idSucursal);
        sucursal.getSucursalServicioCollection();
        for (SucursalServicio sucursalServicio : sucursal.getSucursalServicioCollection()) {

            Especialidad especialidad = sucursalServicio.getIdServicio().getIdEspecialidad();
            List<HashMap> arrayList = new LinkedList<>();

            for (Servicio servicio : especialidad.getServicioCollection()) {
                HashMap<String, Object> aux = new HashMap<>();
                aux.put("idServicio", servicio.getIdServicio());
                aux.put("nombre", servicio.getNombre());
                aux.put("idSucursalServicio", sucursalServicio.getIdSucursalServicio());
                arrayList.add(aux);
            }
            h.put("serviciosList", arrayList);
            h.put("nombre", especialidad.getNombre());
            h.put("idEspecialidad", especialidad.getIdEspecialidad());

        }

        return h;
    }
}
