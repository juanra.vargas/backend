/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.HashMap;
import java.util.LinkedList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.PersonaSucursalServicio;
import py.com.prueba.modelo.SucursalServicio;

/**
 *
 * @author juanvargas2096
 */
@Stateless
public class SucursalServicioEJB {

    @PersistenceContext(unitName = "pwbiiPU")
    private EntityManager em;

    protected EntityManager getEm() {
        return em;
    }

    public SucursalServicio get(Integer id) {
        return (SucursalServicio) getEm().find(SucursalServicio.class, id);
    }

    public HashMap getProfesionales(Integer idSucursalServicio) {
        HashMap<String, Object> h = new HashMap<>();
        LinkedList<Persona> listEntity = new LinkedList<>();
        SucursalServicio sucursalServicio = this.get(idSucursalServicio);
        for (PersonaSucursalServicio personaSucursalServicio : sucursalServicio.getPersonaSucursalServicioCollection()) {
            Persona persona = personaSucursalServicio.getIdEmpleado();
            if (persona.getFlagEmpleado().equals("S")) {
                listEntity.add(persona);
            }
            System.out.println(persona);
        }
        h.put("lista", listEntity);
        return h;
    }
}
