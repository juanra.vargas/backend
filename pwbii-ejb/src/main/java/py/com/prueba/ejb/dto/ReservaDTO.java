/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb.dto;

/**
 *
 * @author juanvargas2096
 */
public class ReservaDTO {

    String fecha;
    String horaInicio;
    String horaFin;
    Integer idSucursalServicio;
    Integer idEmpleado;
    String idCliente;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Integer getIdSucursalServicio() {
        return idSucursalServicio;
    }

    public void setIdSucursalServicio(Integer idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public ReservaDTO() {

    }

    @Override
    public String toString() {
        return "ReservaDTO{" + "fecha=" + fecha + ", horaInicio=" + horaInicio + ", horaFin=" + horaFin + ", idSucursalServicio=" + idSucursalServicio + ", idEmpleado=" + idEmpleado + ", idCliente=" + idCliente + '}';
    }

}
