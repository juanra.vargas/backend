/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb.dto;

import java.io.Serializable;

/**
 *
 * @author juanvargas2096
 */
public class SucursalDTO implements Serializable {

    String nombreSucursal;
    String nombreCiudad;
    String direccion;
    String nombreLocal;

    public SucursalDTO() {
    }

    public SucursalDTO(String nombreSucursal, String nombreCiudad, String direccion, String nombreLocal) {
        this.nombreSucursal = nombreSucursal;
        this.nombreCiudad = nombreCiudad;
        this.direccion = direccion;
        this.nombreLocal = nombreLocal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombreLocal() {
        return nombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }
}
