/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.PersistenceContext;

/**
 *
 * @author juanvargas2096
 */
@Singleton
@Startup
public class TheSingleton {

    @PersistenceContext(unitName = "pwbiiPU")
    @EJB
    ReservaEJB reservaEJB;

    @PostConstruct
    public void inicio() {
        System.out.println("OUT: iniciando");
    }

    @Schedule(second = "0", minute = "*/5", hour = "*", persistent = false)
    public void actualizarAsistencias() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = dateFormat.format(calendar.getTime());
        System.out.println(fecha);
        System.out.println("Barriendo..........");
        Object executeUpdate = reservaEJB.getEm().createNativeQuery("update\n"
                + "	reserva\n"
                + "set\n"
                + "	flag_asistio = 'N'\n"
                + "where\n"
                + "	flag_estado = 'R'\n"
                + "	and fecha = '" + fecha + "' and flag_asistio is not distinct\n"
                + "from\n"
                + "	null;")
                .executeUpdate();
        System.out.println(executeUpdate);
    }
}
