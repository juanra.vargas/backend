/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb.dto;

/**
 *
 * @author juanvargas2096
 */
public class PersonaDTO {

    String correo;

    @Override
    public String toString() {
        return "PersonaDTO{" + "correo=" + correo + '}';
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public PersonaDTO() {
    }

}
