/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.prueba.ejb.dto.SucursalDTO;
import py.com.prueba.modelo.Sucursal;

/**
 *
 * @author juanvargas2096
 */
@Stateless
public class SucursalEJB {

    @PersistenceContext(unitName = "pwbiiPU")
    private EntityManager em;

    protected EntityManager getEm() {
        return em;
    }

    public Sucursal get(Integer id) {
        return em.find(Sucursal.class, id);
    }

    public void persist(Sucursal entity) {
        getEm().persist(entity);
    }

    public Sucursal merge(Sucursal entity) {
        return (Sucursal) getEm().merge(entity);
    }

    public void delete(Integer id) {
        Sucursal entity = this.get(id);
        this.getEm().remove(entity);
    }

    @SuppressWarnings("unchecked")
    public List<Sucursal> lista() {
        Query q = getEm().createQuery(
                "SELECT s FROM Sucursal s");
        return (List<Sucursal>) q.getResultList();
    }

    /*public List<SucursalDTO> listarDTO(Integer idCategoria) {
        Query query = getEm().createQuery("select\n"
                + "  sucursal.nombre,\n"
                + "  ciudad.nombre,\n"
                + "  mapa.direccion,\n"
                + "  local.nombre\n"
                + "from\n"
                + "  sucursal \n"
                + "  join mapa on mapa.id_mapa = sucursal.id_mapa\n"
                + "  join ciudad  on ciudad.id_ciudad = sucursal.id_ciudad\n"
                + "  join local  on local.id_local = sucursal.id_local\n"
                + "  and local.id_local in (\n"
                + "    select\n"
                + "      id_local\n"
                + "    from\n"
                + "      categoria_local\n"
                + "    where\n"
                + "      id_local = :id\n"
                + "  )");

        return (List<SucursalDTO>) query.setParameter("id", idCategoria).getResultList();
    }*/
}
