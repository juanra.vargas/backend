/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import py.com.prueba.ejb.dto.ReservaDTO;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;

/**
 *
 * @author juanvargas2096
 */
@Stateless
public class ReservaEJB {

    @EJB
    SucursalServicioEJB sucursalServicioEJB;

    @EJB
    PersonaEJB personaEJB;
    @PersistenceContext(unitName = "pwbiiPU")
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public Reserva find(Integer id) {
        return (Reserva) getEm().find(Reserva.class, id);
    }

    private Date stringToFecha(String fecha) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = formatter.parse(fecha);
        return date;
    }

    private String diaDeLaSemana(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("EEEE");
        String diaDeLaSemana = dateFormat.format(fecha);

        if (diaDeLaSemana.equals("Saturday")) {
            diaDeLaSemana = "sabado";
        } else if (diaDeLaSemana.equals("Sunday")) {
            diaDeLaSemana = "domingo";
        } else if (diaDeLaSemana.equals("Monday")) {
            diaDeLaSemana = "lunes";
        } else if (diaDeLaSemana.equals("Tuesday")) {
            diaDeLaSemana = "martes";
        } else if (diaDeLaSemana.equals("Wednesday")) {
            diaDeLaSemana = "miercoles";
        } else if (diaDeLaSemana.equals("Thursday")) {
            diaDeLaSemana = "jueves";
        } else if (diaDeLaSemana.equals("Friday")) {
            diaDeLaSemana = "viernes";
        }
        return diaDeLaSemana;
    }

    /**
     *
     * Tema 5
     *
     * @param idSucursalServicio
     * @param fecha
     * @return
     * @throws java.text.ParseException
     */
    @SuppressWarnings({"unchecked", "UnusedAssignment"})
    public ArrayList<Map<String, String>> getHorariosDisponibles(Integer idSucursalServicio, String fecha) throws ParseException {
        Date date = this.stringToFecha(fecha);
        String diaDeLaSemana = this.diaDeLaSemana(date);

        SucursalServicio sucursalServicio = sucursalServicioEJB.get(idSucursalServicio);
        Sucursal sucursal = sucursalServicio.getIdSucursal();

        Time horaApertura = (Time) getEm().createQuery("Select s." + diaDeLaSemana + "HoraApertura from Sucursal s WHERE s=:sucursal").setParameter("sucursal", sucursal).getSingleResult();
        Time horaCierre = (Time) getEm().createQuery("Select s." + diaDeLaSemana + "HoraCierre from Sucursal s WHERE s=:sucursal").setParameter("sucursal", sucursal).getSingleResult();

        Integer duracion = sucursalServicio.getDuracion();
        Integer capacidad = sucursalServicio.getCapacidad();

        LocalTime horaDeAperturaLocalTime = horaApertura.toLocalTime();
        LocalTime horaDeCierreLocalTime = horaCierre.toLocalTime();
        LocalTime iterLocalTime = horaDeAperturaLocalTime;

        ArrayList<Map<String, String>> mapaResultado = new ArrayList<>();

        try {
            while ((iterLocalTime.plusMinutes(duracion)).compareTo(horaDeCierreLocalTime) <= 0) {

                //numero total de reseras con flag R 
                Object reservasTotalesObject = (Object) getEm()
                        .createNativeQuery("select count(r) from reserva r where r.id_sucursal_servicio = :sucserv and r.fecha = :fecha and r.hora_inicio = :hapertura and r.hora_fin = :hcierre and r.flag_estado = 'R';")
                        .setParameter("sucserv", sucursalServicio.getIdSucursalServicio())
                        .setParameter("fecha", date, TemporalType.DATE)
                        .setParameter("hapertura", Time.valueOf(iterLocalTime))
                        .setParameter("hcierre", Time.valueOf(iterLocalTime.plusMinutes(duracion))).getSingleResult();
                Long reservasTotales = Long.parseLong(String.valueOf(reservasTotalesObject));

                //si existe una exepción del horario
                Object excepcionesObject = (Object) getEm()
                        .createNativeQuery("select\n"
                                + "	count(h)\n"
                                + "from\n"
                                + "	horario_excepcion h\n"
                                + "where\n"
                                + "	h.id_sucursal =:suc\n"
                                + "	and h.fecha =:fecha\n"
                                + "	and h.hora_apertura =:hapertura\n"
                                + "	and h.hora_cierre =:hcierre")
                        .setParameter("suc", sucursal)
                        .setParameter("fecha", date, TemporalType.DATE)
                        .setParameter("hapertura", Time.valueOf(iterLocalTime))
                        .setParameter("hcierre", Time.valueOf(iterLocalTime.plusMinutes(duracion)))
                        .getSingleResult();
                Long excepciones = Long.parseLong(String.valueOf(excepcionesObject));

                if (excepciones == 0 && reservasTotales <= capacidad) {
                    Map<String, String> temp = new HashMap<>();
                    temp.put("fecha", fecha);
                    temp.put("horeInicio", iterLocalTime.toString());
                    iterLocalTime = iterLocalTime.plusMinutes(duracion);
                    temp.put("horaSalida", iterLocalTime.toString());
                    mapaResultado.add(temp);
                } else {
                    iterLocalTime = iterLocalTime.plusMinutes(duracion);
                }

            }
        } catch (NumberFormatException e) {
            System.out.println(e);
            mapaResultado = new ArrayList<>();
        }

        return mapaResultado;
    }

    //Tema 6
    public ArrayList<Map<String, String>> getHorariosDisponiblesPorEmpleado(Integer idSucursalServicio, String fecha, Integer idEmpleado) throws ParseException {
        Date date = this.stringToFecha(fecha);
        String diaDeLaSemana = this.diaDeLaSemana(date);

        Persona empleado = personaEJB.get(idEmpleado);
        System.out.println("Empleado: ........");
        System.out.println(empleado);
        if (empleado == null) {
            return new ArrayList();
        }
        SucursalServicio sucursalServicio = sucursalServicioEJB.get(idSucursalServicio);
        Sucursal sucursal = sucursalServicio.getIdSucursal();
        Time horaApertura = (Time) getEm().createQuery("Select s." + diaDeLaSemana + "HoraApertura from Sucursal s WHERE  s=:sucursal").setParameter("sucursal", sucursal).getSingleResult();
        Time horaCierre = (Time) getEm().createQuery("Select s." + diaDeLaSemana + "HoraCierre from Sucursal s WHERE s=:sucursal").setParameter("sucursal", sucursal).getSingleResult();
        System.out.println(horaApertura);
        System.out.println(horaCierre);
        Integer duracion = sucursalServicio.getDuracion();
        Integer capacidad = sucursalServicio.getCapacidad();

        System.out.println(duracion);
        System.out.println(capacidad);
        LocalTime horaDeAperturaLocalTime = horaApertura.toLocalTime();
        LocalTime horaDeCierreLocalTime = horaCierre.toLocalTime();
        LocalTime iterLocalTime = horaDeAperturaLocalTime;

        ArrayList<Map<String, String>> mapaResultado = new ArrayList<>();

        try {
            while ((iterLocalTime.plusMinutes(duracion)).compareTo(horaDeCierreLocalTime) <= 0) {

                Object reservasTotalesObject = (Object) getEm()
                        .createNativeQuery("select count(r) from reserva r where r.id_sucursal_servicio = :sucserv and r.fecha = :fecha and r.hora_inicio = :hapertura and r.hora_fin = :hcierre and r.flag_estado = 'R';")
                        .setParameter("sucserv", sucursalServicio.getIdSucursalServicio())
                        .setParameter("fecha", date, TemporalType.DATE)
                        .setParameter("hapertura", Time.valueOf(iterLocalTime))
                        .setParameter("hcierre", Time.valueOf(iterLocalTime.plusMinutes(duracion))).getSingleResult();
                Long reservasTotales = Long.parseLong(String.valueOf(reservasTotalesObject));
                System.out.println(reservasTotales);

                Time hcierre = Time.valueOf(iterLocalTime.plusMinutes(duracion));
                System.out.println(hcierre);
                //numero total de reseras con flag R 
                Object reservasPorEmpleadoObject = (Object) getEm()
                        .createNativeQuery("select\n"
                                + "	count(r)\n"
                                + "from\n"
                                + "	reserva r\n"
                                + "where\n"
                                + "	r.id_sucursal_servicio = :sucserv\n"
                                + "	and r.id_empleado = :empleado\n"
                                + "	and r.fecha = :fecha\n"
                                + "	and r.hora_inicio = :hapertura\n"
                                + "	and r.hora_fin = :hcierre\n"
                                + "	and r.flag_estado = 'R';")
                        .setParameter("empleado", empleado.getIdPersona())
                        .setParameter("sucserv", sucursalServicio.getIdSucursalServicio())
                        .setParameter("fecha", date, TemporalType.DATE)
                        .setParameter("hapertura", Time.valueOf(iterLocalTime))
                        .setParameter("hcierre", hcierre).getSingleResult();
                Long reservasPorEmpleado = Long.parseLong(String.valueOf(reservasPorEmpleadoObject));
                System.out.println(reservasPorEmpleado);

                //si existe una exepción del horario
                Object excepcionesObject = (Object) getEm()
                        .createNativeQuery("select\n"
                                + "	count(h)\n"
                                + "from\n"
                                + "	horario_excepcion h\n"
                                + "where\n"
                                + "	h.id_sucursal =:suc\n"
                                + "	and h.fecha =:fecha\n"
                                + "	and h.hora_apertura =:hapertura\n"
                                + "	and h.hora_cierre =:hcierre")
                        .setParameter("suc", sucursal)
                        .setParameter("fecha", date, TemporalType.DATE)
                        .setParameter("hapertura", Time.valueOf(iterLocalTime))
                        .setParameter("hcierre", Time.valueOf(iterLocalTime.plusMinutes(duracion)))
                        .getSingleResult();
                Long excepciones = Long.parseLong(String.valueOf(excepcionesObject));
                System.out.println(excepciones);

                if (excepciones == 0 && reservasTotales <= capacidad && reservasPorEmpleado <= 1) {
                    Map<String, String> temp = new HashMap<>();
                    temp.put("fecha", fecha);
                    temp.put("horeInicio", iterLocalTime.toString());
                    iterLocalTime = iterLocalTime.plusMinutes(duracion);
                    temp.put("horaSalida", iterLocalTime.toString());
                    mapaResultado.add(temp);
                } else {
                    iterLocalTime = iterLocalTime.plusMinutes(duracion);
                }
            }
        } catch (NumberFormatException e) {
            System.out.println(e);
            mapaResultado = new ArrayList<>();
        }

        return mapaResultado;
    }

    // TEMA 8
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public List<Reserva> obtenerTodasLasReservas(
            String fechaDesde,
            String fechaHasta,
            String horaDesde,
            String horaHasta,
            Integer idServicio,
            Integer idEspecialidad,
            Integer idEmpleado,
            Integer idLocal,
            Integer idSucursal,
            String estado,
            String asistio,
            Integer offset,
            Integer limit) {

        String query = "SELECT r FROM Reserva r "
                + " JOIN r.idSucursalServicio ss "
                + " JOIN ss.idServicio sr "
                + " JOIN ss.idSucursal sc "
                + " LEFT JOIN r.idEmpleado p "
                + " JOIN sc.local l "
                + " JOIN sr.idEspecialidad es "
                + "WHERE 1 = 1";

        if (fechaDesde != null) {
            query = query.concat(" and r.fecha >= '".concat(fechaDesde)).concat("'");

        }
        if (fechaHasta != null) {
            query = query.concat(" and r.fecha <= '".concat(fechaHasta)).concat("'");
        }
        if (horaDesde != null) {
            query = query.concat(" and r.horaInicio >= '".concat(horaDesde)).concat("'");
        }

        if (horaHasta != null) {
            query = query.concat(" and r.horaInicio <= '".concat(horaHasta)).concat("'");
        }
        if (idServicio != null) {
            query = query.concat(" and id_servicio <= '".concat(idServicio.toString())).concat("'");
        }
        if (idEspecialidad != null) {
            query = query.concat(" and id_especialidad <= '".concat(idEspecialidad.toString())).concat("'");
        }
        if (idEmpleado != null) {
            query = query.concat(" and id_empleado <= '".concat(idEmpleado.toString())).concat("'");
        }
        if (idLocal != null) {
            query = query.concat(" and id_local <= '".concat(idLocal.toString())).concat("'");
        }
        if (idSucursal != null) {
            query = query.concat(" and id_sucursal <= '").concat(idSucursal.toString()).concat("'");
        }
        if (estado != null) {
            query = query.concat(" and estado <= '").concat(estado).concat("'");
        }
        if (asistio != null) {
            query = query.concat(" and asistio <= '").concat(asistio).concat("'");
        }

        Query q = getEm().createQuery(query);

        if (offset != null) {
            q = q.setFirstResult(offset);
        }
        if (limit != null) {
            q = q.setMaxResults(limit);
        }

        return (List<Reserva>) q.getResultList();

    }

    public Reserva get(Integer id) {
        return (Reserva) getEm().find(Reserva.class, id);
    }

    // TEMA 9
    public Reserva actualizar(Reserva r) {

        int cambios = 0;
        Integer id = r.getIdReserva();
        System.out.println(r.getIdReserva());
        if (r.getIdReserva() == null) {
            System.out.println("Sin idReserva");
            return new Reserva();
        }
        Reserva reserva = this.get(r.getIdReserva());
        if (reserva.getFlagAsistio() != null) {
            System.out.println("FlagAsistio ya cargado");
            return new Reserva();
        }
        if (r.getFlagEstado().equals("C") || r.getFlagEstado().equals("c") && r.getObservacion() == null) {
            System.out.println("Sin justificacion");
            return new Reserva();
        }
        Date fecha = reserva.getFecha();
        if (fecha.compareTo(new Date()) > 0) {
            System.out.println("Fecha pasada");
            return new Reserva();
        }
        if (fecha.compareTo(new Date()) > 0) {
            System.out.println("Hora pasada");
            return new Reserva();
        }
        String query = "Update Reserva SET ";

        if (r.getFlagEstado() != null) {
            query += "flagEstado = '" + r.getFlagEstado()
                    + "' ";
            cambios++;
        }
        if (r.getFlagAsistio() != null) {
            if (cambios > 0) {
                query += ", ";
            }
            query += "flagAsistio = '" + r.getFlagAsistio() + "' ";
            cambios++;
        }

        if (r.getObservacion() != null) {
            if (cambios > 0) {
                query += ", ";
            }
            query += "observacion = '" + r.getObservacion() + "' ";
            cambios++;
        }

        if (cambios > 0) {
            query += "WHERE idReserva = " + id;
            getEm().createQuery(query).executeUpdate();

        }

        return this.find(id);
    }

    //Tema 7
    public Reserva guardarReserva(ReservaDTO r) throws ParseException {
        System.out.println("Iniciando guardarReserva");
        if (r.getFecha() != null && r.getHoraInicio() != null && r.getHoraFin() != null && r.getIdSucursalServicio() != null && r.getIdCliente() != null && verificarDisponibilidad(r.getIdEmpleado())) {
            Reserva reserva = new Reserva();
            reserva.setFecha(this.stringToFecha(r.getFecha()));
            reserva.setHoraInicio(this.stringToFecha(r.getHoraInicio()));
            reserva.setHoraFin(this.stringToFecha(r.getHoraFin()));
            reserva.setIdSucursalServicio(this.find(r.getIdSucursalServicio()).getIdSucursalServicio());
            System.out.println(r.getIdEmpleado());
            if (r.getIdEmpleado() != null) {
                reserva.setIdEmpleado(personaEJB.get(r.getIdEmpleado()));
                System.out.println("Empleado");
                System.out.println(reserva.getIdEmpleado());
            }
            System.out.println(r.getIdCliente());
            Object idPersonaObject = getIdPersona(r.getIdCliente());
            System.out.println(idPersonaObject);
            if (idPersonaObject == null) {
                return new Reserva();
            }
            System.out.println("idPersonaObject");
            System.out.println(idPersonaObject);
            Long idPersona = Long.parseLong(String.valueOf(idPersonaObject));

            System.out.println(idPersona);
            reserva.setIdCliente(personaEJB.get(idPersona.intValue()));

            System.out.println("Guardando");
            try {
                if (consultarDisponibilidad(reserva)) {
                    reserva.setIdReserva(getNextValueReserva());
                    reserva.setFechaHoraCreacion(new Date());
                    reserva.setFlagEstado("R");
                    reserva.setObservacion("");
                    getEm().persist(reserva);
                    return reserva;
                } else {
                    return new Reserva();
                }
            } catch (ParseException ex) {
                System.out.println(ex);
            }
        }
        return new Reserva();
    }

    private Object getIdPersona(String correo) {
        Object id = (Object) getEm().createNativeQuery("select\n"
                + "	id_persona\n"
                + "from\n"
                + "	persona p\n"
                + "where\n"
                + "	p.email = '" + correo + "'").getFirstResult();
        System.out.println(id);
        return id;
    }

    private boolean verificarDisponibilidad(Integer idEmpleado) {

        Object valorObject = (Object) getEm().createNativeQuery("select\n"
                + "	count(r)\n"
                + "from\n"
                + "	reserva r\n"
                + "where\n"
                + "	r.id_empleado = :empleado\n"
                + "	and r.flag_estado = 'R';")
                .setParameter("empleado", idEmpleado)
                .getSingleResult();
        Long valor = Long.parseLong(String.valueOf(valorObject));
        return valor.intValue() == 0;
    }

    private Integer getNextValueReserva() {
        Object nroFilasObject = (Object) this.getEm().createNativeQuery("select\n"
                + "	count(r)\n"
                + "from\n"
                + "	reserva r;").getSingleResult();

        Long nroFilas = Long.parseLong(String.valueOf(nroFilasObject));
        System.out.println("NroFilas: ");
        System.out.println(nroFilas);
        return (nroFilas.intValue() + 1);
    }

    private boolean consultarDisponibilidad(Reserva r) throws ParseException {

        Date date = r.getFecha();
        SucursalServicio sucursalServicio = r.getIdSucursalServicio();
        Sucursal sucursal = sucursalServicio.getIdSucursal();

        Integer capacidad = sucursalServicio.getCapacidad();
        try {

            Object reservasTotalesObject = (Object) getEm()
                    .createNativeQuery("select\n"
                            + "	count(r)\n"
                            + "from\n"
                            + "	reserva r\n"
                            + "where\n"
                            + "	r.id_sucursal_servicio = :sucserv\n"
                            + "	and r.fecha = :fecha\n"
                            + "	and r.hora_inicio = :hapertura\n"
                            + "	and r.hora_fin = :hcierre\n"
                            + "	and r.flag_estado = 'R';")
                    .setParameter("sucserv", sucursalServicio.getIdSucursalServicio())
                    .setParameter("fecha", date, TemporalType.DATE)
                    .setParameter("hapertura", r.getHoraInicio())
                    .setParameter("hcierre", r.getHoraFin()).getSingleResult();
            Long reservasTotales = Long.parseLong(String.valueOf(reservasTotalesObject));

            //si existe una exepción del horario
            Object excepcionesObject = (Object) getEm()
                    .createNativeQuery("select\n"
                            + "	count(h)\n"
                            + "from\n"
                            + "	horario_excepcion h\n"
                            + "where\n"
                            + "	h.id_sucursal =:suc\n"
                            + "	and h.fecha =:fecha\n"
                            + "	and h.hora_apertura =:hapertura\n"
                            + "	and h.hora_cierre =:hcierre")
                    .setParameter("suc", sucursal)
                    .setParameter("fecha", date, TemporalType.DATE)
                    .setParameter("hapertura", r.getHoraInicio())
                    .setParameter("hcierre", r.getHoraFin())
                    .getSingleResult();
            Long excepciones = Long.parseLong(String.valueOf(excepcionesObject));

            if (excepciones == 0 && reservasTotales <= capacidad) {
                return true;
            }

        } catch (NumberFormatException e) {
            System.out.println(e);
        }
        return false;
    }

}
