package py.com.prueba.ejb;

import java.util.LinkedList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import py.com.prueba.ejb.dto.SucursalDTO;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Sucursal;

@Stateless
public class CategoriaEJB {

    @PersistenceContext(unitName = "pwbiiPU")
    private EntityManager em;

    protected EntityManager getEm() {
        return em;
    }

    public Categoria get(Integer id) {
        return em.find(Categoria.class, id);
    }

    public void persist(Categoria entity) {
        getEm().persist(entity);
    }

    public Categoria merge(Categoria entity) {
        return (Categoria) getEm().merge(entity);
    }

    public void delete(Integer id) {
        Categoria entity = this.get(id);
        this.getEm().remove(entity);
    }

    public void delete(Categoria entity) {
        this.delete(entity.getIdCategoria());
    }

    @SuppressWarnings("unchecked")
    public List<Categoria> lista() {
        Query q = getEm().createQuery(
                "SELECT p FROM Categoria p");
        return (List<Categoria>) q.getResultList();
    }

    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Categoria p");
        return (Long) q.getSingleResult();
    }

    public List<Sucursal> listaSucursal(Integer idCategoria) {
        Session session = (Session) em.getDelegate();
        Criteria c = session.createCriteria(Sucursal.class);

        Criteria cCategoria = c.createCriteria("local", "l").createCriteria("l.categoriaCollection", "c");
        cCategoria.add(Restrictions.eq("c.idCategoria", idCategoria));
        return cCategoria.list();
    }

    public List<SucursalDTO> listarSucursalesNative(Integer idCategoria) {

        System.out.println(idCategoria);
        Query q = getEm().createNativeQuery("select\n"
                + "	s.nombre as nombreSucursal,\n"
                + "	c.nombre as nombreCiudad,\n"
                + "	m.direccion as direccion,\n"
                + "	l.nombre as nombreLocal\n"
                + "from\n"
                + "	sucursal s\n"
                + "join mapa m on\n"
                + "	m.id_mapa = s.id_mapa\n"
                + "join ciudad c on\n"
                + "	c.id_ciudad = s.id_ciudad\n"
                + "join local l on\n"
                + "	l.id_local = s.id_local\n"
                + "	and l.id_local in (\n"
                + "	select\n"
                + "		id_local\n"
                + "	from\n"
                + "		categoria_local\n"
                + "	where\n"
                + "		id_categoria = :idCategoria);")
                .setParameter("idCategoria", idCategoria);

        List<Object[]> name = q.getResultList();
        List<SucursalDTO> list = new LinkedList<>();
        for (Object[] objects : name) {
            SucursalDTO sucursalDTO = new SucursalDTO();
            sucursalDTO.setNombreSucursal((String) objects[0]);
            sucursalDTO.setNombreCiudad((String) objects[1]);
            sucursalDTO.setDireccion((String) objects[2]);
            sucursalDTO.setNombreLocal((String) objects[3]);
            list.add(sucursalDTO);
        }
        //return (List<SucursalDTO[]>) name;
        return list;
    }
}
