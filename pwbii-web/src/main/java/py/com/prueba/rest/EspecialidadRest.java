/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.rest;

import java.util.TreeMap;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import py.com.prueba.ejb.EspecialidadEJB;

/**
 *
 * @author juanvargas2096
 */
@Path("especialidad")
@Produces("application/json")
@Consumes("application/json")
public class EspecialidadRest {

    @Inject
    private EspecialidadEJB especialidadEJB;

    @GET
    @Path("/listar")
    public Response listar(@QueryParam("idSucursal") Integer idSucursal) throws WebApplicationException {
        TreeMap listEntity;
        listEntity = especialidadEJB.listar(idSucursal);
        //Map<String, Object> mapaResultado = new HashMap<String, Object>();
        //mapaResultado.put("lista", listEntity);
        return Response.ok(listEntity).build();
    }

}
