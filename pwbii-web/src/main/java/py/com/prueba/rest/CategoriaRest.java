/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import py.com.prueba.ejb.CategoriaEJB;
import py.com.prueba.ejb.dto.SucursalDTO;

import py.com.prueba.modelo.Categoria;

/**
 *
 * @author guille
 */
@Path("categoria")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class CategoriaRest {

    @Inject
    private CategoriaEJB categoriaEJB;
    @Context
    protected UriInfo uriInfo;

    @GET
    @Path("/")

    public Response listar() throws WebApplicationException {

        List<Categoria> listEntity = null;
        Long total = null;
        total = categoriaEJB.total();
        listEntity = categoriaEJB.lista();
        Map<String, Object> mapaResultado = new HashMap<String, Object>();
        mapaResultado.put("total", total);
        mapaResultado.put("lista", listEntity);

        return Response.ok(mapaResultado).build();

    }

    @GET
    @Path("/sucursal")

    public Response listar(@QueryParam("idCategoria") Integer idCategoria) throws WebApplicationException {

        /*List<Sucursal> listEntity = null;
        Long total = null;
        
        listEntity = categoriaEJB.listaSucursal(pk);
        total = (long) listEntity.size();
        Map<String,Object> mapaResultado=new HashMap<String, Object>();
        mapaResultado.put("total", total);
        mapaResultado.put("lista", listEntity);*/
        List<SucursalDTO> listEntity = null;
        listEntity = categoriaEJB.listarSucursalesNative(idCategoria);
        Map<String, Object> mapaResultado = new HashMap<String, Object>();
        mapaResultado.put("lista", listEntity);
        mapaResultado.put("total", 1);
        return Response.ok(mapaResultado).build();

    }

    @GET
    @Path("/{pk}")
    public Response obtener(@PathParam("pk") Integer pk) {
        Categoria entityRespuesta = null;
        entityRespuesta = categoriaEJB.get(pk);
        return Response.ok(entityRespuesta).build();
    }

    @POST
    @Path("/")
    public Response crear(Categoria entity) throws WebApplicationException {

        categoriaEJB.persist(entity);

        UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
                .getAbsolutePath());
        URI resourceUri = null;
        try {
            resourceUri = resourcePathBuilder
                    .path(URLEncoder.encode(entity.getIdCategoria().toString(), "UTF-8")).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.created(resourceUri).build();
    }

    @PUT
    @Path("/")
    public Response modificar(Categoria entity) throws WebApplicationException {
        categoriaEJB.merge(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{pk}")
    public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {

        categoriaEJB.delete(pk);
        return Response.ok().build();

    }
}
