/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import py.com.prueba.ejb.SucursalEJB;
import py.com.prueba.ejb.dto.SucursalDTO;

/**
 *
 * @author juanvargas2096
 */
@Path("sucursal")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class SucursalRest {

    @Inject
    SucursalEJB sucursalEJB;
    @Context
    protected UriInfo uriInfo;

    @GET
    @Path("/categoria")
    public Response listarPorCategoria(@QueryParam("id") Integer id) {

        List<SucursalDTO> sucursalesDTO;
        //sucursalesDTO = sucursalEJB.listarDTO(id);
        Map<String, Object> mapaResultado = new HashMap<String, Object>();
        // FmapaResultado.put("lista", sucursalesDTO);
        return Response.ok(mapaResultado).build();
    }

}
