/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.rest;

import java.util.HashMap;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.prueba.ejb.SucursalServicioEJB;

/**
 *
 * @author juanvargas2096
 */
@Path("/sucursal-servicio")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SucursalServicioRest {

    @Inject
    SucursalServicioEJB sucursalServicioEJB;

    @GET
    @Path("/profesionales")
    public Response listarProfesionales(@QueryParam("idSucursalServicio") Integer idSucursalServicio) {
        HashMap listEntity;
        listEntity = sucursalServicioEJB.getProfesionales(idSucursalServicio);
        //Map<String, Object> mapaResultado = new HashMap<String, Object>();
        //mapaResultado.put("lista", listEntity);
        return Response.ok(listEntity).build();
    }

}
