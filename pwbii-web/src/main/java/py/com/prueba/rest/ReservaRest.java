/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.prueba.ejb.ReservaEJB;
import py.com.prueba.ejb.dto.ReservaDTO;
import py.com.prueba.modelo.Reserva;

/**
 *
 * @author juanvargas2096
 */
@Path("/reserva")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ReservaRest {

    @Inject
    private ReservaEJB reservaEJB;

    @GET
    @Path("/disponible")
    public Response listar(@QueryParam("idSucursalServicio") Integer idSucursalServicio, @QueryParam("fecha") String fecha) throws ParseException, WebApplicationException {
        ArrayList<Map<String, String>> listEntity = null;
        listEntity = reservaEJB.getHorariosDisponibles(idSucursalServicio, fecha);

        Map<String, Object> mapaResultado = new HashMap<>();

        mapaResultado.put("horarios", listEntity);

        return Response.ok(mapaResultado).build();

    }

    @GET
    @Path("/disponible-por-empleado")
    public Response listarPorEmpleado(@QueryParam("idSucursalServicio") Integer idSucursalServicio, @QueryParam("fecha") String fecha, @QueryParam("idEmpleado") Integer idEmpleado) throws ParseException, WebApplicationException {
        ArrayList<Map<String, String>> listEntity = null;
        listEntity = reservaEJB.getHorariosDisponiblesPorEmpleado(idSucursalServicio, fecha, idEmpleado);

        Map<String, Object> mapaResultado = new HashMap<>();

        mapaResultado.put("horarios", listEntity);

        return Response.ok(mapaResultado).build();
    }

    @GET
    //@Path("/listar-todas-las-reservas")
    public Response listar(
            @QueryParam("fechaDesde") String fecha_desde,
            @QueryParam("fechaHasta") String fecha_hasta,
            @QueryParam("horaDesde") String hora_desde,
            @QueryParam("horaHasta") String hora_hasta,
            @QueryParam("idServicio") Integer id_servicio,
            @QueryParam("idEspecialidad") Integer id_especialidad,
            @QueryParam("idEmpleado") Integer id_empleado,
            @QueryParam("idLocal") Integer id_local,
            @QueryParam("idsucursal") Integer id_sucursal,
            @QueryParam("estado") String estado,
            @QueryParam("asistio") String asistio,
            @QueryParam("offset") Integer offset,
            @QueryParam("limit") Integer limit
    ) throws WebApplicationException {
        System.out.println(fecha_desde);

        List list = reservaEJB.obtenerTodasLasReservas(fecha_desde, fecha_hasta, hora_desde, hora_hasta, id_servicio, id_especialidad, id_empleado, id_local, id_sucursal, estado, asistio, offset, limit);
        return Response.ok(list).build();

    }

    @PUT
    public Response actualizar(Reserva reserva) {
        return Response.ok(reservaEJB.actualizar(reserva)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response guardar(ReservaDTO reserva) throws ParseException {
        return Response.ok(reservaEJB.guardarReserva(reserva)).build();
    }

    @GET
    @Path("/horarios-por-empleado")
    public Response horariosPorEmpleado(@QueryParam("idSucursalServicio") Integer idSucursalServicio, @QueryParam("fecha") String fecha, @QueryParam("idEmpleado") Integer idEmpleado) throws ParseException {
        return Response.ok(reservaEJB.getHorariosDisponiblesPorEmpleado(idSucursalServicio, fecha, idEmpleado)
        ).build();
    }
}
